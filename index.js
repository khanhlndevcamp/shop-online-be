import express from "express"
import mongoose from "mongoose"

//import router
import productTypeRouter from "./app/routes/productTypeRouter.js"
import productRouter from "./app/routes/productRouter.js"
import customerRouter from "./app/routes/customerRouter.js"
import orderRouter from "./app/routes/orderRouter.js"
import orderDetailRouter from "./app/routes/orderDetailRouter.js"
import cors from 'cors'
import voucherRouter from "./app/routes/voucherRouter.js"
import accountRouter from "./app/routes/accountRouter.js"
import loginRouter from "./app/routes/loginRouter.js"
import dotenv from 'dotenv'

dotenv.config()

//initialize app
const app = express()

//initialize port
const port = 8000

//connect mongoose
// mongoose.connect("mongodb://localhost:27017/CRUD_Shop24h")
console.log('line 26 process.env.HOST_DATE_BASE_ATLAS', process.env.HOST_DATE_BASE_ATLAS)
mongoose.connect(process.env.HOST_DATE_BASE_ATLAS,
    { useNewUrlParser: true, useUnifiedTopology: true }
    )
    .then(() => {
        console.log('Success to connect MongoDB')
    })
    .catch((err) => {
        console.log('err connect')
        throw err
    })

//middleware
app.use(express.json())
app.use(cors())



//creat api 
app.get('/', (req, res) => {
    res.status(200).json({
        message: `successfully get api shop24h on port ${port} `
    })
})

app.use(productTypeRouter)
app.use(productRouter)
app.use(customerRouter)
app.use(orderRouter)
app.use(orderDetailRouter)
app.use(voucherRouter)
app.use(accountRouter)
app.use(loginRouter)

//run app
app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})