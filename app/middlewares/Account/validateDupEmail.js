import customerModel from "../../models/customerModel.js";

export const validateDupEmail = (req, res, next) => {
    //get data
    console.log('req bodyyyyyyyyyyyyy', req.body)
    let email = req.body.email
    let currentEmail = req.query.currentEmail ? req.query.currentEmail : ''

    customerModel.findOne({ email: email })
        .then((customer) => {
            if (currentEmail === email) {
                next()
            } else {
                if (customer) {
                    return res.status(400).json({ message: "Failed! Email is already in use!" });
                }
                next()
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}