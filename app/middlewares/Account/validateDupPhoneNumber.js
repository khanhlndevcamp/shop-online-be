import customerModel from "../../models/customerModel.js";

export const validateDupPhoneNumber = (req, res, next) => {
        //get data
        let phone = req.body.phone
        let currentPhone = req.query.currentPhone ? req.query.currentPhone : ''

        customerModel.findOne({ phone: phone })
            .then((customer) => {
                if(currentPhone === phone) {
                    next()
                }
                else {
                    if (customer) {
                        return res.status(400).json({ message: "Failed! Phone number is already in use!" });
                    }
                    next()
                }
            })
            .catch((err) => {
                res.status(500).json({
                    message: `Internal server error: ${err}`
                })
            })
}