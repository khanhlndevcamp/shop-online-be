import accountModel from "../../models/accountModel.js"

export const validateDupUsername = (req, res, next) => {
    //get data
    let username = req.body.username
    let currentUsername = req.query.currentUsername ? req.query.currentUsername : ''

    accountModel.findOne({ username: username })
        .then((user) => {
            if(username === currentUsername) {
                next()
            } else {
                if (user) {
                    return res.status(400).json({ message: "Failed! Username is already in use!" });
                }
                next()
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}