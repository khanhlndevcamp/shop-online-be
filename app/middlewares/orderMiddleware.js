//function kiểm tra định dạng ngày tháng năm
function validateOrderDate(req, res, next) {
    let body = req.body
    
    //neu khong co du lieu orderdate thi next
    if(body.orderDate == undefined) {
        console.log('validate order date next')
        return next()
    }
    let dateValue = body.orderDate
    //chuyển dữ liệu ngày về array và gán giá trị vào day month year tương ứng
    var [month, day, year] = dateValue.split('/');
    //kiểm tra nếu nhập vào tháng có 1 chữ số thì thêm 0 vào trước
    if (month.length < 2) {
        month = '0' + month
    }
    var newDateValue = `${day}/${month}/${year}`
    //định dạng ngày mm/dd/yyyy
    var vFormData = /^\d{2}\/\d{2}\/\d{4}$/;
    //kiểm tra xem text nhập vào có đúng định dạng dd/mm/yyyy hay không
    if (newDateValue.match(vFormData) === null) {
        return res.status(400).json({
            message: 'orderDate is invalid'
        })
    }
    // định dạng lại ngày theo format yyyy-mm-dd để tạo được ngày mới
    var vDateStringNewFormart = `${year}-${month}-${day}`;
    var vDate = new Date(vDateStringNewFormart); //tạo ngày mới, nếu số ngày nhập không đúng như 35/1/2019 sẽ không tạo được trả về invalid date
    var vTimeStamp = vDate.getTime(); //lấy số mili giây của ngày vừa tạo tính từ 00:00:00 1/1/1970, nếu vDate là invalid thì vTimeStamp = NaN
    //kiểm tra vTimeStamp
    if (typeof vTimeStamp !== 'number' || Number.isNaN(vTimeStamp)) {
        return res.status(400).json({
            message: 'orderDate is invalid'
        })
    }
    next()
}

function validateShippedDate(req, res, next) {
    let body = req.body
    
    //neu khong co du lieu shippedDate thi next
    if(body.shippedDate == undefined) {
        console.log('validate order date next')
        return next()
    }
    let dateValue = body.shippedDate
    //chuyển dữ liệu ngày về array và gán giá trị vào day month year tương ứng
    var [month, day, year] = dateValue.split('/');
    //kiểm tra nếu nhập vào tháng có 1 chữ số thì thêm 0 vào trước
    if (month.length < 2) {
        month = '0' + month
    }
    var newDateValue = `${day}/${month}/${year}`
    //định dạng ngày dd/mm/yyyy
    var vFormData = /^\d{2}\/\d{2}\/\d{4}$/;
    //kiểm tra xem text nhập vào có đúng định dạng dd/mm/yyyy hay không
    if (newDateValue.match(vFormData) === null) {
        return res.status(400).json({
            message: 'shippedDate is invalid'
        })
    }
    // định dạng lại ngày theo format yyyy-mm-dd để tạo được ngày mới
    var vDateStringNewFormart = `${year}-${month}-${day}`;
    var vDate = new Date(vDateStringNewFormart); //tạo ngày mới, nếu số ngày nhập không đúng như 35/1/2019 sẽ không tạo được trả về invalid date
    var vTimeStamp = vDate.getTime(); //lấy số mili giây của ngày vừa tạo tính từ 00:00:00 1/1/1970, nếu vDate là invalid thì vTimeStamp = NaN
    //kiểm tra vTimeStamp
    if (typeof vTimeStamp !== 'number' || Number.isNaN(vTimeStamp)) {
        return res.status(400).json({
            message: 'shippedDate is invalid'
        })
    }
    next()
}

export {validateOrderDate, validateShippedDate}