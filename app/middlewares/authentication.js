import jwt from 'jsonwebtoken'

export const authentication = (req, res, next) => {

    try {
        const token = req.headers.authentication.split(' ')[1]
        //payload is accountId
        const resultCheckToken = jwt.verify(token, process.env.SECRET_KEY)
        req.accountId = resultCheckToken.accountId
        next()
    }
    catch (err) {
        console.log('err authentication')
        return res.status(403).json({
            message: 'Need sign in',
            err: err
        })
    }
}