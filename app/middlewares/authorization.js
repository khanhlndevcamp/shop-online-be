import accountModel from "../models/accountModel.js"


export const authorization = (...role) => {
    return (req, res, next) => {
        try {
            console.log('authorization start')
            accountModel.findById(req.accountId)
                .then(account => {
                    if(role.flat(Infinity).includes(account.role)) {
                        console.log('authorization: have permission')
                        next()
                    } else {
                        console.log('authorization: dont have permission')
                        return res.status(403).json({
                            message: 'Do not have permission to access'
                        })
                    }
                })
                .catch(err => {
                    return res.status(403).json({
                        message: `Internal server error: ${err}`
                    })
                })  
        }
        catch (err) {
            console.log('err authorization')
            return res.status(403).json({
                message: 'Need sign in'
            })
        }
    }
}

