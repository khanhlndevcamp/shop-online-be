import mongoose from "mongoose";
import orderDetailModel from "../models/orderDetailModel.js";
import orderModel from "../models/orderModel.js";
import productModel from "../models/productModel.js";

//creat order detail 
const creatOrderDetail = (req, res) => {
    //1. get data
    let body = req.body
    let idOrder = req.params.idOrder
    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idOrder)) {
        return res.status(400).json({
            message: 'idOrder is invalid'
        })
    }
    if (body.product != undefined && !mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            message: 'product is invalid'
        })
    }
    if (body.quantity != undefined && !Number.isInteger(body.quantity)) {
        return res.status(400).json({
            message: 'quanity is invalid'
        })
    }
    //3. process
    let newOrderDetail = new orderDetailModel({
        _id: mongoose.Types.ObjectId(),
        product: body.product,
        quantity: body.quantity,
    })
    orderDetailModel.create(newOrderDetail)
        .then((data) => {
            //update order details of order
            orderModel.findByIdAndUpdate(idOrder, { $push: { orderDetails: data._id } })
                .then((updatedOrder) => {
                    //get product by id
                    productModel.findById(body.product)
                        .then((productById) => {
                            let newProduct = {
                                quantitySold: productById.quantitySold + body.quantity,
                                $push: { orderDetails: data._id },
                                amount: productById.amount - body.quantity
                            }
                            console.log('line 46 newProduct:', newProduct)
                            //update order details of product
                            productModel.findByIdAndUpdate(body.product, newProduct)
                                .then((updatedProduct) => {
                                    res.status(201).json({
                                        message: 'creat new order detail successfully!',
                                        orderDetail: data
                                    })
                                })
                                .catch((err) => {
                                    res.status(500).json({
                                        message: `Internal sever error: ${err}`
                                    })
                                })
                        })
                })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err}`
                    })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get all order detail 
const getAllOrderDetail = (req, res) => {
    //3. process
    orderDetailModel.find()
        .then((data) => {
            res.status(201).json({
                message: 'get all order detail successfully!',
                allOrderDetail: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get order detail by id
const getOrderDetailById = (req, res) => {
    //1. get data
    let idOrderDetail = req.params.idOrderDetail

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idOrderDetail)) {
        return res.status(400).json({
            message: 'idOrderDetail is invalid'
        })
    }

    //3. process
    orderDetailModel.findById(idOrderDetail)
        .populate('product')
        .then((data) => {
            res.status(201).json({
                message: 'get order detail by id successfully!',
                orderDetail: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get order detail of order
const getOrderDetailOfOrder = (req, res) => {
    //1.get data
    let idOrder = req.params.idOrder

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idOrder)) {
        return res.status(400).json({
            message: 'idOrder is invalid'
        })
    }

    //.3 process
    orderModel.findById(idOrder)
        .populate('voucher')
        .then((order) => {
            let condition = { _id: [] }
            order.orderDetails.forEach((orderDetail) => {
                condition._id.push(orderDetail._id)
            })
            orderDetailModel.find(condition)
                .populate('product')
                .then((orderEject) => {
                    res.status(200).json({
                        message: 'get order detail of order successfully',
                        order: order,
                        orderDetails: orderEject
                    })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}
//update order detail 
const updateOrderDetail = (req, res) => {
    //1. get data
    let idOrderDetail = req.params.idOrderDetail
    let body = req.body

    //.2 validate
    if (!mongoose.Types.ObjectId.isValid(idOrderDetail)) {
        return res.status(400).json({
            message: 'idOrderDetail is invalid'
        })
    }
    if (body.product != undefined && !mongoose.Types.ObjectId.isValid(body.product)) {
        return res.status(400).json({
            message: 'product is invalid'
        })
    }
    if (body.quantity != undefined && !Number.isInteger(body.quantity)) {
        return res.status(400).json({
            message: 'quantity is invalid'
        })
    }

    //3. process
    let newOrderDetail = {
        product: body.product,
        quantity: body.quantity,
    }
    orderDetailModel.findByIdAndUpdate(idOrderDetail, newOrderDetail)
        .then((prevOrderDetail) => {
            //get product by id
            productModel.findById(body.product)
                .populate('orderDetails')
                .then(productById => {
                    let newQuantitySold = productById.orderDetails.reduce((acc, orderDetailItem) => {
                        return acc += +orderDetailItem.quantity
                    }, 0)
                    let newAmount = +productById.amount + +productById.quantitySold - +newQuantitySold
                    let newProduct = {
                        quantitySold: +newQuantitySold,
                        amount: newAmount
                    }
                    console.log('line 197 newProduct:', newProduct)
                    //update product quantity sold
                    productModel.findByIdAndUpdate(body.product, newProduct)
                        .then((updatedProduct) => {
                            res.status(201).json({
                                message: 'update order detail by id successfully!',
                                prevOrderDetail: prevOrderDetail,
                                newOrderDetail: newOrderDetail
                            })
                        })
                        .catch((err) => {
                            res.status(500).json({
                                message: `Internal sever error: ${err}`
                            })
                        })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//delete order detail 
const deleteOrderDetail = (req, res) => {
    //1. get data
    let { idOrderDetail, idOrder } = req.params

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idOrderDetail)) {
        return res.status(400).json({
            message: 'idOrderDetail is invalid'
        })
    }
    if (!mongoose.Types.ObjectId.isValid(idOrder)) {
        return res.status(400).json({
            message: 'idOrder is invalid'
        })
    }
    console.log('======line 233 start delete=======')
    //3. process
    console.log('line 235 idOrderDetail', idOrderDetail)
    console.log('line 236 idOrder', idOrder)
    orderDetailModel.findByIdAndDelete(idOrderDetail)
        .then((data) => {
            //update order
            console.log('line 238 data OrderDetail', data)

            orderModel.findByIdAndUpdate(idOrder, { $pull: { orderDetails: data._id } })
                .then((updatedOrder) => {
                    //update order detail of product model
                    productModel.findByIdAndUpdate(data.product, { $pull: { orderDetails: data._id } })
                        .then(updatedProduct => {
                            console.log('line 247 updated product:', updatedProduct)
                            //get product by id
                            productModel.findById(data.product)
                                .populate('orderDetails')
                                .then(productById => {
                                    console.log('line 252 productById:', productById)
                                    let newQuantitySold = productById.orderDetails.reduce((acc, orderDetailItem) => {
                                        return acc += orderDetailItem.quantity
                                    }, 0)
                                    let newAmount = +productById.amount + +productById.quantitySold - +newQuantitySold
                                    console.log('line 256: test quantitySold', newQuantitySold)
                                    // let newQuantitySold = +productById.quantitySold - +data.quantity
                                    let newProduct = {
                                        quantitySold: +newQuantitySold,
                                        amount :newAmount
                                    }
                                    console.log('new product line 255:', newProduct)
                                    //update product quantity sold
                                    productModel.findByIdAndUpdate(data.product, newProduct)
                                        .then((updatedProductQuantitySold) => {
                                            res.status(204).json({
                                                message: 'delete order detail by id successfully!',
                                            })
                                        })
                                        .catch((err) => {
                                            res.status(500).json({
                                                message: `Internal sever error: ${err}`
                                            })
                                        })
                                })
                        })
                })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err}`
                    })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

export { creatOrderDetail, getAllOrderDetail, getOrderDetailById, getOrderDetailOfOrder, updateOrderDetail, deleteOrderDetail }