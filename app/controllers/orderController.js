import mongoose from "mongoose";
import orderModel from "../models/orderModel.js";
import customerModel from "../models/customerModel.js";
import orderDetailModel from "../models/orderDetailModel.js";
import productModel from "../models/productModel.js";

//creat order 
const creatOrder = (req, res) => {
    //1. get data
    let body = req.body
    let idCustomer = req.params.idCustomer
    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idCustomer)) {
        return res.status(400).json({
            message: 'idCustomer is invalid'
        })
    }
    if (body.note != undefined && Number.isInteger(body.note)) {
        return res.status(400).json({
            message: 'note is invalid'
        })
    }
    if (body.cost != undefined && !Number.isInteger(body.cost)) {
        return res.status(400).json({
            message: 'cost is invalid'
        })
    }
    if (body.voucher !== undefined && body.voucher !== '' && !mongoose.Types.ObjectId.isValid(body.voucher)) {
        return res.status(400).json({
            message: 'voucher is invalid'
        })
    }
    if (!body.customerName) {
        return res.status(400).json({
            message: 'Customer name is invalid'
        })
    }
    if (!body.phoneNumber) {
        return res.status(400).json({
            message: 'Phone number is invalid'
        })
    }
    if (!body.email) {
        return res.status(400).json({
            message: 'Email customer is invalid'
        })
    }
    if (!body.address) {
        return res.status(400).json({
            message: 'Address customer is invalid'
        })
    }
    //3. process
    let newOrder = new orderModel({
        _id: mongoose.Types.ObjectId(),
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost,
        customerName: body.customerName,
        phoneNumber: body.phoneNumber,
        email: body.email,
        address: body.address,
        totalQuantity: body.totalQuantity
    })

    const progressOrder = [
        {
            status: 'created order',
            step: 1,
            progressPercent: 5
        },
        {
            status: 'waiting for confirmation',
            step: 2,
            progressPercent: 23
        },
        {
            status: 'confirmed',
            step: 3,
            progressPercent: 40
        },
        {
            status: 'packed',
            step: 4,
            progressPercent: 57
        },
        {
            status: 'shipping',
            step: 5,
            progressPercent: 76
        },
        {
            status: 'delivered',
            step: 6,
            progressPercent: 100
        },
    ]

    if (body.voucher) {
        newOrder.voucher = body.voucher
    } else {
        newOrder.voucher = null
    }

    if (body.status) {
        newOrder.status = body.status
        progressOrder.forEach((progress) => {
            if (body.status.toLocaleLowerCase().trim() === progress.status.toLocaleLowerCase().trim()) {
                newOrder.step = progress.step
                newOrder.progressPercent = progress.progressPercent
            }
        })
    }
    orderModel.create(newOrder)
        .then((data) => {
            //get info customer by customer id
            customerModel.findById(idCustomer)
                .then((customerById) => {
                    let newCustomer = {
                        totalOrder: customerById.totalOrder + 1,
                        totalReveneu: customerById.totalReveneu + body.cost,
                        $push: { orders: data._id }
                    }
                    //update total order, reveneu and order list of customer
                    customerModel.findByIdAndUpdate(idCustomer, newCustomer)
                        .then((updatedCustomer) => {
                            res.status(201).json({
                                message: 'creat new order successfully!',
                                order: data
                            })
                        })
                        .catch((err) => {
                            res.status(500).json({
                                message: `Internal sever error: ${err}`
                            })
                        })
                })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err}`
                    })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get all order 
const getAllOrder = (req, res) => {
    //3. process
    orderModel.find()
        .then((data) => {
            let orderDescendingByDate = data.sort((a, b) => b.createdAt - a.createdAt)

            res.status(201).json({
                message: 'get all orders successfully!',
                allOrders: orderDescendingByDate
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get order overview
const getOrderOverview = (req, res) => {

    orderModel.find()
        .populate('orderDetails')
        .then(data => {
            const dataOverview = {}
            const totalOrders = data.length
            const pendingOrders = data.filter(order => +order.step < 6).length
            const completedOrders = data.filter(order => +order.step === 6).length
            const totalReveneu = data.reduce((acc, order) => { return acc += order.cost }, 0)
            let totalSoldProducts = 0
            data.forEach((order) => {
                const quantitySoldOfOrder = order.orderDetails.reduce((acc, orderDetail) => {
                    return acc += orderDetail.quantity
                }, 0)
                totalSoldProducts += quantitySoldOfOrder
            })
            dataOverview.totalOrders = totalOrders
            dataOverview.pendingOrders = pendingOrders
            dataOverview.completedOrders = completedOrders
            dataOverview.completedOrders = completedOrders
            dataOverview.totalReveneu = totalReveneu
            dataOverview.totalSoldProducts = totalSoldProducts

            console.log('order controller, dataoverview', dataOverview)
            res.status(200).json({
                message: 'get order overview success',
                data: dataOverview
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get order by order code
const getOrderByOrderCode = (req, res) => {
    //1 get data
    let orderCode = req.params.orderCode
    console.log('order code', orderCode)

    orderModel.findOne({ orderCode: orderCode })
        .populate('voucher')
        .then(dataOrder => {
            console.log('dataorder', dataOrder)
            const condition = { _id: [] }
            dataOrder.orderDetails.forEach((orderDetail) => {
                condition._id.push(orderDetail._id)
            })
            orderDetailModel.find(condition)
                .populate('product')
                .then(dataOrderDetail => {
                    res.status(202).json({
                        message: 'get order by order code successfully!',
                        order: dataOrder,
                        orderDetail: dataOrderDetail
                    })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get order by filter
const getOrderByFitler = (req, res) => {
    let filterConditionQuery = req.query
    let orderCodeQuery = filterConditionQuery.orderCode ? filterConditionQuery.orderCode : ''
    let emailQuery = filterConditionQuery.email ? filterConditionQuery.email : ''
    let statusQuery = filterConditionQuery.statusOrder ? filterConditionQuery.statusOrder : ''
    let fromDateQuery = filterConditionQuery.fromDate ? filterConditionQuery.fromDate : '' //date format mm-dd-yyyy
    let toDateQuery = filterConditionQuery.toDate ? filterConditionQuery.toDate : '' //date format mm-dd-yyyy
    let page = !filterConditionQuery.page ? 1 : filterConditionQuery.page
    let limit = !filterConditionQuery.limit ? 10 : filterConditionQuery.limit
    let orderCodeSort = filterConditionQuery.orderCodeSort ? filterConditionQuery.orderCodeSort : ''
    let priceSort = filterConditionQuery.priceSort ? filterConditionQuery.priceSort : ''
    let orderQuantitySort = filterConditionQuery.orderQuantitySort ? filterConditionQuery.orderQuantitySort : ''
    let createDateSort = filterConditionQuery.createDateSort ? filterConditionQuery.createDateSort : ''

    let sortCondition = {}

    if (orderCodeSort !== '') {
        sortCondition.orderCode = +orderCodeSort
    }
    if (priceSort !== '') {
        sortCondition.cost = +priceSort
    }
    if (orderQuantitySort !== '') {
        sortCondition.totalQuantity = +orderQuantitySort
    }
    if (createDateSort !== '') {
        sortCondition.createdAt = +createDateSort
    }

    let filterCondition = {
        orderCode: { "$regex": orderCodeQuery, "$options": "i" },
        email: { "$regex": emailQuery, "$options": "i" },
    }

    if (statusQuery !== '') {
        filterCondition.status = statusQuery
    }
    if (fromDateQuery !== '' && toDateQuery !== '') {
        filterCondition.createdAt = { $gte: new Date(fromDateQuery), $lte: new Date(toDateQuery) }
    }

    console.log('line 263 sort condition:', sortCondition)
    console.log('line 263 filter condition:', filterCondition)
    orderModel.find(filterCondition)
        .populate('orderDetails')
        .then((data) => {
            // const filterConditionCustomer = {email: {"$regex": [], "$option": "i"}}
            // customerModel.find()
            // find({ airedAt: { $gte: '1987-10-19', $lte: '1987-10-26' } })
            // createdAt
            // orderModel.find({createdAt: { $gte: new Date('2023-05-20'), $lte: new Date('2023-05-22') }})
            let totalPrice = data.reduce((acc, order) => {
                return acc += +order.cost
            }, 0)
            let listOrderDetails = data.map((order) => {
                return [...order.orderDetails]
            }).flat(Infinity)
            let totalQuantitySold = listOrderDetails.reduce((acc, orderDetail) => {
                return acc += +orderDetail.quantity
            }, 0)

            let totalOrders = data.length
            orderModel.find(filterCondition)
                .populate('orderDetails')
                .sort(sortCondition)
                .limit(limit)
                .skip(limit * (page - 1))
                .then(dataPage => {
                    let numberPage = Math.ceil(+data.length / +limit)
                    res.status(201).json({
                        message: 'get order by filter successfully!',
                        orders: dataPage,
                        numberPage,
                        overal: {
                            totalPrice,
                            totalQuantitySold,
                            totalOrders
                        }
                    })

                })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err}`
                    })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get order by id
const getOrderById = (req, res) => {
    //1. get data
    let idOrder = req.params.idOrder

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idOrder)) {
        return res.status(400).json({
            message: 'idOrder is invalid'
        })
    }

    //3. process
    orderModel.findById(idOrder)
        .populate('orderDetails')
        .then((data) => {
            res.status(201).json({
                message: 'get order by id successfully!',
                order: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get order of customer
const getOrdersOfCustomer = (req, res) => {
    //1. get data
    let idCustomer = req.params.idCustomer

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idCustomer)) {
        return res.status(400).json({
            message: 'idCustomer is invalid'
        })
    }
    //3. process
    customerModel.findById(idCustomer)
        .populate('orders')
        .then((customer) => {
            res.status(200).json({
                message: 'get orders of customer successfully',
                orders: customer.orders
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get order of customer by customer email
const getOrdersOfCustomerByCustomerEmail = (req, res) => {
    //1. get data
    let emailCustomer = req.params.emailCustomer
    let page = req.query.page ? req.query.page : 1
    let limit = req.query.limit ? req.query.limit : 5
    let orderCodeQuery = req.query.orderCode

    //3. process
    customerModel.findOne({ email: emailCustomer })
        .populate('orders')
        .then((customer) => {
            let orderDescendingByDate = customer.orders.sort((a, b) => b.createdAt - a.createdAt)
            if (orderCodeQuery) {
                orderDescendingByDate = orderDescendingByDate.filter((order) => {
                    return order.orderCode.includes(orderCodeQuery)
                })
            }
            let orderResponse = orderDescendingByDate.slice((page - 1) * limit, page * limit)
            // let orderResponse = orderDescendingByDate.slice(0, 5)
            let numberPage = Math.ceil(orderDescendingByDate.length / limit)
            res.status(200).json({
                message: 'get orders of customer successfully',
                customerInfo: customer,
                orders: orderResponse,
                numberPage
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get total reveu of order of each month
const getTotalReveneuOfOrderOfEachMonth = (req, res) => {
    //get data
    let year = req.params.year
    let listOfMonths = []
    for (let i = 0; i < 12; i++) {
        let dataMonth = {
            month: `${i + 1}-1-${year}`, //format mm-dd-yyyy
            reveneu: 0,
            listOrders: []
        }
        listOfMonths.push(dataMonth)
    }
    let fromDate = `1/1/${year}` //format mm-dd-yyyy
    let toDate = `12/31/${year}`//format mm-dd-yyyy
    orderModel.find({ createdAt: { $gte: new Date(fromDate), $lte: new Date(toDate) } })
        .then((dataOrder) => {
            listOfMonths.forEach((month) => {
                let filterTime = new Date(month.month)
                let filterMonth = `${filterTime.getMonth() + 1}-${filterTime.getFullYear()}`
                dataOrder.forEach(order => {
                    let orderTime = new Date(order.createdAt)
                    let orderMonth = `${orderTime.getMonth() + 1}-${orderTime.getFullYear()}`
                    if (filterMonth === orderMonth) {
                        // month.listOrders.push(order)
                        month.reveneu += +order.cost
                    }
                })
            })

            res.status(200).json({
                message: 'getTotalReveneuOfOrderOfEachMonth success',
                listReveneuOfMonth: listOfMonths
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })

}

//get total reveu of order of each day
const getTotalReveneuOfOrderOfEachDay = (req, res) => {
    //get data
    console.log('line 471 getTotalReveneuOfOrderOfEachDay')
    let year = req.params.year
    let month = req.params.month
    let listReveneuDaysInMonth = []
    let totalDayOfMonth = new Date(year, month, 0).getDate()
    for (let i = 0; i < totalDayOfMonth; i++) {

        let dataDay = {
            day: new Date(year, month - 1, i + 1), //format mm-dd-yyyy
            // day: `${month}-${i + 1}-${year}`, //format mm-dd-yyyy
            reveneu: 0,
        }
        listReveneuDaysInMonth.push(dataDay)
    }
    let fromDate = `${month}-1-${year}` //if month is 5, new date is 2023-04-30 format yyyy-mm-dd
    let toDate = '' //if month is 5, new date is 2023-06-01 format yyyy-mm-dd
    if (month == 12) {
        toDate = `1-2-${+year + 1}`
    } else {
        toDate = `${+month + 1}-2-${year}`
    }
    // let toDate = new Date(`${month}-${totalDayOfMonth}-${year}`).toDateString()
    // orderModel.find({ createdAt: { $gte: listReveneuDaysInMonth[0].day, $lte: listReveneuDaysInMonth[listReveneuDaysInMonth.length - 1].day } })
    orderModel.find({ createdAt: { $gte: new Date(fromDate), $lte: new Date(toDate) } })
        // orderModel.find()
        .then((dataOrder) => {
            // console.log('dataOrder.createdAt:', dataOrder[dataOrder.length -1])
            // console.log('dataOrder.createdAt:', dataOrder[dataOrder.length -1].createdAt)
            // console.log('dataOrder.createdAt:', new Date(dataOrder[dataOrder.length -1].createdAt).getUTCDate())
            console.log('listReveneuDaysInMonth[0]):', listReveneuDaysInMonth[0])
            console.log('listReveneuDaysInMonth[listReveneuDaysInMonth.length -1]):', listReveneuDaysInMonth[listReveneuDaysInMonth.length -1])
            console.log('new Date(listReveneuDaysInMonth[0].day):', new Date(listReveneuDaysInMonth[0].day))
            console.log('first date time:', new Date(listReveneuDaysInMonth[0].day).getDate())
            console.log('new Date(listReveneuDaysInMonth[listReveneuDaysInMonth.length -1].day)):', new Date(listReveneuDaysInMonth[listReveneuDaysInMonth.length -1].day))
            console.log('last date time:', new Date(listReveneuDaysInMonth[listReveneuDaysInMonth.length -1].day).getDate())
            // console.log('filter time:', new Date(listReveneuDaysInMonth[listReveneuDaysInMonth.length -2].day).getMonth())
            listReveneuDaysInMonth.forEach((day) => {
                let filterTime = new Date(day.day)
                //getUTCDate to get exactly date of day
                // let filterMonth = `${filterTime.getMonth() + 1}-${filterTime.getUTCDate()}-${filterTime.getFullYear()}` //format mm-dd-yyyy
                let filterMonth = `${month}-${filterTime.getUTCDate()}-${filterTime.getFullYear()}` //format mm-dd-yyyy
                dataOrder.forEach(order => {
                    let orderTime = new Date(order.createdAt)
                    let orderMonth = `${orderTime.getMonth() + 1}-${orderTime.getUTCDate()}-${orderTime.getFullYear()}`
                    // console.log('=================')
                    // console.log('filterMonth', filterMonth)
                    // console.log('orderMonth', orderMonth)
                    // console.log('compar', filterMonth === orderMonth)
                    if (filterMonth === orderMonth) {
                        day.reveneu += +order.cost
                    }
                })
            })
            console.log('listReveneuDaysInMonth', listReveneuDaysInMonth)
            res.status(200).json({
                message: 'getTotalReveneuOfOrderOfEachDay success',
                listReveneuDaysInMonth
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })

}

//update order 
const updateOrder = (req, res) => {
    //1. get data
    let idOrder = req.params.idOrder
    let body = req.body

    console.log('1 get data update order', idOrder, body)
    //.2 validate
    if (!mongoose.Types.ObjectId.isValid(idOrder)) {
        return res.status(400).json({
            message: 'idOrder is invalid'
        })
    }
    if (body.note != undefined && Number.isInteger(body.note)) {
        return res.status(400).json({
            message: 'note is invalid'
        })
    }
    if (body.cost != undefined && !Number.isInteger(body.cost)) {
        return res.status(400).json({
            message: 'cost is invalid'
        })
    }
    console.log('result check voucher:', body.voucher != '')

    if (body.voucher != undefined && body.voucher != '' && !mongoose.Types.ObjectId.isValid(body.voucher)) {
        return res.status(400).json({
            message: 'voucher is invalid'
        })
    }

    const progressOrder = [
        {
            status: 'created order',
            step: 1,
            progressPercent: 5
        },
        {
            status: 'waiting for confirmation',
            step: 2,
            progressPercent: 23
        },
        {
            status: 'confirmed',
            step: 3,
            progressPercent: 40
        },
        {
            status: 'packed',
            step: 4,
            progressPercent: 57
        },
        {
            status: 'shipping',
            step: 5,
            progressPercent: 76
        },
        {
            status: 'delivered',
            step: 6,
            progressPercent: 100
        },
    ]

    //3. process
    let newOrder = {
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost,
        status: body.status,
        customerName: body.customerName,
        phoneNumber: body.phoneNumber,
        email: body.email,
        address: body.address,
        totalQuantity: body.totalQuantity
    }

    if (body.voucher) {
        newOrder.voucher = body.voucher
    } else {
        newOrder.voucher = null
    }

    progressOrder.forEach((progress) => {
        if (body.status.toLocaleLowerCase().trim() === progress.status.toLocaleLowerCase().trim()) {
            newOrder.step = progress.step
            newOrder.progressPercent = progress.progressPercent
        }
    })


    orderModel.findByIdAndUpdate(idOrder, newOrder)
        .then((prevOrder) => {
            //get info customer by customer id
            customerModel.findOne({ orders: idOrder })
                .populate('orders')
                .then((customerById) => {
                    let idCustomer = customerById._id
                    let totalOrder = customerById.length
                    let totalReveneu = customerById.orders.reduce((acc, order) => {
                        return acc += +order.cost
                    }, 0)
                    let newCustomer = {
                        totalOrder: totalOrder,
                        totalReveneu: totalReveneu,
                    }
                    //update total order, reveneu and order list of customer
                    customerModel.findByIdAndUpdate(idCustomer, newCustomer)
                        .then((updatedCustomer) => {
                            res.status(201).json({
                                message: 'update order by id successfully!',
                                prevOrder: prevOrder,
                                newOrder: newOrder
                            })
                        })
                        .catch((err) => {
                            res.status(500).json({
                                message: `Internal sever error: ${err}`
                            })
                        })
                })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err}`
                    })
                })

        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//delete order 
const deleteOrder = (req, res) => {
    //1. get data
    let { idCustomer, idOrder } = req.params

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idCustomer)) {
        return res.status(400).json({
            message: 'idCustomer is invalid'
        })
    }
    if (!mongoose.Types.ObjectId.isValid(idOrder)) {
        return res.status(400).json({
            message: 'idOrder is invalid'
        })
    }

    //3. process
    orderModel.findByIdAndDelete(idOrder)
        .then((data) => {
            orderDetailModel.deleteMany({ _id: [...data.orderDetails] })
                .then(dataOrderDetail => {
                    //get info customer by customer id
                    customerModel.findById(idCustomer)
                        .then(customerById => {
                            let newCustomer = {}
                            if(customerById) {
                                newCustomer = {
                                    totalOrder: customerById.totalOrder - 1,
                                    totalReveneu: customerById.totalReveneu - data.cost,
                                    $pull: { orders: data._id }
                                }
                            }
                            //update total order, reveneu, order list
                            customerModel.findByIdAndUpdate(idCustomer, newCustomer)
                                .then((updatedCustomer) => {
                                    res.status(204).json({
                                        message: 'delete order by id successfully!',
                                    })
                                })
                                .catch((err) => {
                                    res.status(500).json({
                                        message: `line 708 Internal sever error: ${err}`
                                    })
                                })
                        })
                        .catch((err) => {
                            console.log('err order controller', err)
                                res.status(500).json({
                                    message: `line 708 Internal sever error: ${err}`
                                })
                        })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `line 727 Internal sever error: ${err}`
            })
        })
}


export {
    creatOrder,
    getAllOrder,
    getOrderById,
    getOrderByOrderCode,
    getOrdersOfCustomer,
    updateOrder,
    deleteOrder,
    getOrdersOfCustomerByCustomerEmail,
    getOrderOverview,
    getOrderByFitler,
    getTotalReveneuOfOrderOfEachMonth,
    getTotalReveneuOfOrderOfEachDay
}