import mongoose from "mongoose";
import customerModel from "../models/customerModel.js";
import accountModel from "../models/accountModel.js";


//creat customer 
const creatCustomer = (req, res) => {
    //1. get data
    let body = req.body

    //2. validate
    if (!body.fullName) {
        return res.status(400).json({
            message: 'fullName is required'
        })
    }
    if (Number.isInteger(body.fullName)) {
        return res.status(400).json({
            message: 'fullName is invalid'
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            message: 'phone is required'
        })
    }
    if (Number.isInteger(body.phone)) {
        return res.status(400).json({
            message: 'phone is invalid'
        })
    }
    if (!body.email) {
        return res.status(400).json({
            message: 'email is required'
        })
    }
    if (Number.isInteger(body.email)) {
        return res.status(400).json({
            message: 'email is invalid'
        })
    }
    if (body.address != undefined && Number.isInteger(body.address)) {
        return res.status(400).json({
            message: 'address is invalid'
        })
    }
    if (body.city != undefined && Number.isInteger(body.city)) {
        return res.status(400).json({
            message: 'city is invalid'
        })
    }
    if (body.country != undefined && Number.isInteger(body.country)) {
        return res.status(400).json({
            message: 'country is invalid'
        })
    }

    //3. process
    let newCustomer = new customerModel({
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
    })

    // let condition = {
    //     phone: body.phone, 
    //     email: body.email
    // }

    //check whether customer exist in db
    customerModel.findOne({ $or: [{ phone: body.phone }, { email: body.email }] })
        .then((data) => {
            // not yet
            if (data === null) {
                customerModel.create(newCustomer)
                    .then((newCustomerData) => {
                        res.status(201).json({
                            message: 'creat new customer successfully!',
                            customer: newCustomerData
                        })
                    })
            } else {//already
                res.status(201).json({
                    message: 'customer already exist!',
                    customer: data
                })
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get all customer 
const getAllCustomer = (req, res) => {
    //3. process
    customerModel.find()
        .then((data) => {
            res.status(200).json({
                message: 'get all customers successfully!',
                allCustomers: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get customer by id
const getCustomerById = (req, res) => {
    //1. get data
    let idCustomer = req.params.id

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idCustomer)) {
        return res.status(400).json({
            message: 'id is invalid'
        })
    }

    //3. process
    customerModel.findById(idCustomer)
        .then((data) => {
            res.status(200).json({
                message: 'get customer by id successfully!',
                customer: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get number of total customer
const getNumberOfTotalCustomer = (req, res) => {
    customerModel.find()
        .then((data) => {
            res.status(200).json({
                message: 'get number of total customer successfully!',
                numberOfTotalCustomer: data.length
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//update customer 
const updateCustomer = (req, res) => {
    //1. get data
    let idCustomer = req.params.id
    let body = req.body

    //.2 validate
    if (!mongoose.Types.ObjectId.isValid(idCustomer)) {
        return res.status(400).json({
            message: 'id is invalid'
        })
    }
    if (body.fullName != undefined && Number.isInteger(body.fullName)) {
        return res.status(400).json({
            message: 'fullName is invalid'
        })
    }
    if (body.phone != undefined && Number.isInteger(body.phone)) {
        return res.status(400).json({
            message: 'phone is invalid'
        })
    }
    if (body.email != undefined && Number.isInteger(body.email)) {
        return res.status(400).json({
            message: 'email is invalid'
        })
    }
    if (body.address != undefined && Number.isInteger(body.address)) {
        return res.status(400).json({
            message: 'address is invalid'
        })
    }
    if (body.city != undefined && Number.isInteger(body.city)) {
        return res.status(400).json({
            message: 'city is invalid'
        })
    }
    if (body.country != undefined && Number.isInteger(body.country)) {
        return res.status(400).json({
            message: 'country is invalid'
        })
    }

    //3. process
    let newCustomer = {
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
    }
    if (body.active !== '') {
        newCustomer.active = body.active
    }
    customerModel.findByIdAndUpdate(idCustomer, newCustomer)
        .then((prevCustomer) => {
            res.status(201).json({
                message: 'update customer by id successfully!',
                prevCustomer: prevCustomer,
                newCustomer: newCustomer
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//delete customer 
const deleteCustomer = (req, res) => {
    //1. get data
    let idCustomer = req.params.customerId
    let accountId = req.params.accountId

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idCustomer)) {
        return res.status(400).json({
            message: 'id is invalid'
        })
    }

    //3. process
    customerModel.findByIdAndDelete(idCustomer)
        .then((data) => {
            accountModel.findByIdAndUpdate(accountId, { customer: null })
                .then(dataAccount => {
                    res.status(204).json({
                        message: 'delete customer by id successfully!',
                    })
                })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err}`
                    })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

const getCustomerByPhoneNumber = (req, res) => {
    const phoneNumberParam = req.params.phoneNumber

    const condition = {
        phone: phoneNumberParam
    }
    customerModel.findOne(condition)
        .then(data => {
            res.status(200).json({
                message: 'get customer by phone number successfully!',
                data
            })
        })
        .catch(err => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get customer by order id
const getCustomerByOrderId = (req, res) => {
    const orderId = req.params.orderId

    const condition = {
        orders: orderId
    }
    console.log('order id, getCustomerByOrderId', condition)
    customerModel.findOne(condition)
        .then(data => {
            res.status(200).json({
                message: 'get customer by order id successfully!',
                data
            })
        })
        .catch(err => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get customer by filter query
const getCustomerByFitler = (req, res) => {
    let filterConditionQuery = req.query
    let email = filterConditionQuery.email
    let phone = filterConditionQuery.phoneNumber
    let page = !filterConditionQuery.page ? 1 : filterConditionQuery.page
    let limit = !filterConditionQuery.limit ? 10 : filterConditionQuery.limit
    let nameSort = filterConditionQuery.nameSort ? filterConditionQuery.nameSort : ''
    let reveneuSort = filterConditionQuery.reveneuSort ? filterConditionQuery.reveneuSort : ''
    let orderSort = filterConditionQuery.orderSort ? filterConditionQuery.orderSort : ''
    let onlyActive = filterConditionQuery.onlyActive ? filterConditionQuery.onlyActive : ''

    const filterCondition = {
    }
    if (onlyActive == 'true') {
        filterCondition.active = true
    } else if (onlyActive == 'false') {
        filterCondition.active = false
    }
    if (email) {
        filterCondition.email = { "$regex": email, "$options": "i" }
    }

    if (phone) {
        filterCondition.phone = { "$regex": phone, "$options": "i" }
    }

    let sortCondition = {}

    if (nameSort !== '') {
        sortCondition.fullName = +nameSort
    }
    if (reveneuSort !== '') {
        sortCondition.totalReveneu = +reveneuSort
    }
    if (orderSort !== '') {
        sortCondition.totalOrder = +orderSort
    }
    customerModel.find(filterCondition)
        .then(data => {
            customerModel.find(filterCondition)
                .populate('orders')
                .sort(sortCondition)
                .limit(limit)
                .skip(limit * (page - 1))
                .then((dataPage) => {
                    let numberPage = Math.ceil(+data.length / +limit)
                    return res.status(200).json({
                        message: 'get customer by filter successfully!',
                        data: dataPage,
                        numberPage
                    })
                })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err}`
                    })
                })
        })
        .catch(err => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

export const getResultCheckDupEmail = (req, res) => {
    //get data
    const email = req.params.email
    const currentEmail = req.query.currentEmail ? req.query.currentEmail : '' //use to edit customer

    customerModel.findOne({ email })
        .then((customer) => {
            if (currentEmail === email) {
                return res.status(200).json({
                    message: 'email valid',
                    result: true
                })
            } else {
                if (customer) {
                    return res.status(200).json({
                        message: 'email already exist',
                        result: false
                    })
                } else {
                    return res.status(200).json({
                        message: 'email valid',
                        result: true
                    })
                }
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}

export const getResultCheckDupPhone = (req, res) => {
    //get data
    const phone = req.params.phone
    const currentPhone = req.query.currentPhone ? req.query.currentPhone : '' //use to edit customer

    customerModel.findOne({ phone })
        .then((customer) => {
            if (phone === currentPhone) {
                return res.status(200).json({
                    message: 'phone valid',
                    result: true
                })
            } {
                if (customer) {
                    return res.status(200).json({
                        message: 'phone already exist',
                        result: false
                    })
                } else {
                    return res.status(200).json({
                        message: 'phone valid',
                        result: true
                    })
                }
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}

export {
    creatCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomer,
    getCustomerByPhoneNumber,
    getNumberOfTotalCustomer,
    getCustomerByOrderId,
    getCustomerByFitler
}