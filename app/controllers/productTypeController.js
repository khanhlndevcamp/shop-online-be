import mongoose from "mongoose";
import productTypeModel from "../models/productTypeModel.js";

//creat product type
const creatProductType = (req, res) => {
    //1. get data
    let body = req.body

    //2. validate
    if (!body.name) {
        return res.status(400).json({
            message: 'name is required'
        })
    }
    if (Number.isInteger(body.name)) {
        return res.status(400).json({
            message: 'name is invalid'
        })
    }
    if (body.description != undefined && Number.isInteger(body.description)) {
        return res.status(400).json({
            message: 'description is invalid'
        })
    }

    //3. process
    let newProductType = new productTypeModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    })
    productTypeModel.create(newProductType)
        .then((data) => {
            res.status(201).json({
                message: 'creat new product type successfully!',
                productType: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get all product type
const getAllProductType = (req, res) => {
    //3. process
    productTypeModel.find()
        .then((data) => {
            res.status(201).json({
                message: 'get all product types successfully!',
                allProductTypes: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get product type by id
const getProductTypeById = (req, res) => {
    //1. get data
    let idProductType = req.params.id

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idProductType)) {
        return res.status(400).json({
            message: 'id is invalid'
        })
    }

    //3. process
    productTypeModel.findById(idProductType)
        .then((data) => {
            res.status(201).json({
                message: 'get product types by id successfully!',
                productType: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//update product type
const updateProductType = (req, res) => {
    //1. get data
    let idProductType = req.params.id
    let body = req.body

    //.2 validate
    if (!mongoose.Types.ObjectId.isValid(idProductType)) {
        return res.status(400).json({
            message: 'id is invalid'
        })
    }
    if (body.name != undefined && Number.isInteger(body.name)) {
        return res.status(400).json({
            message: 'name is invalid'
        })
    }
    if (body.description != undefined && Number.isInteger(body.description)) {
        return res.status(400).json({
            message: 'description is invalid'
        })
    }

    //3. process
    let newProductType = {
        name: body.name,
        description: body.description
    }
    productTypeModel.findByIdAndUpdate(idProductType, newProductType)
        .then((prevProductType) => {
            res.status(201).json({
                message: 'update product types by id successfully!',
                prevProductType: prevProductType,
                newProductType: newProductType
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//delete product type
const deleteProductType = (req, res) => {
    //1. get data
    let idProductType = req.params.id

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idProductType)) {
        return res.status(400).json({
            message: 'id is invalid'
        })
    }

    //3. process
    productTypeModel.findByIdAndDelete(idProductType)
        .then((data) => {
            res.status(204).json({
                message: 'delete product types by id successfully!',
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

export { creatProductType, getAllProductType, getProductTypeById, updateProductType, deleteProductType }