import accountModel from "../models/accountModel.js"
import jwt from "jsonwebtoken"
import bcrypt from 'bcryptjs'

export const login = (req, res) => {
    let { username, password } = req.body

    accountModel.findOne({ username })
        .populate('customer')
        .then((account) => {
            if (account) {
                
                var passwordIsValid = bcrypt.compareSync(
                    password,
                    account.password
                );

                if (passwordIsValid) {
                    if(account.customer.active) {
                        //asscess token
                        // const token = jwt.sign({ accountId: account._id }, process.env.SECRET_KEY)
                        const token = jwt.sign({ accountId: account._id }, process.env.SECRET_KEY, {expiresIn: '3d'})
    
                        //refresh token
                        // const refreshToken = jwt.sign({ accountId: account._id }, process.env.SECRET_KEY_REFRESH_TOKEN, {expiresIn: '30d'})
                        return res.status(200).json({
                            message: 'login success',
                            token,
                            fullName: account.customer.fullName,
                            imageURL: account.imageURL,
                            // refreshToken
                        }) 
                    } else {
                        return res.status(200).json({
                            message: 'inactive account',
                            token: null
                        })
                    }
                } else {
                    return res.status(200).json({
                        message: 'wrong password',
                        token: null
                    })
                }
            } else {
                return res.status(200).json({
                    message: 'username does not exist',
                    token: null
                })
            }
        })
        .catch(err => {
            return res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}

//check expired date token
export const checkExpiredDateToken = (req, res) => {

    try {
        const token = req.headers.authentication.split(' ')[1]
        //payload is accountId
        const resultCheckToken = jwt.verify(token, process.env.SECRET_KEY)
        return res.status(200).json({
            expired: false
        })        
    }
    catch (err) {
        console.log('err authentication')
        if(err.name === 'TokenExpiredError') {
            return res.status(403).json({
                message: 'Token expired',
                expired: true
            })
        } else {
            return res.status(403).json({
                message: 'Need sign in',
                err
            })
        }
    }
}