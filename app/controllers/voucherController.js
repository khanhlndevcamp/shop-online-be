import mongoose from 'mongoose'
import voucherModel from '../models/voucherModel.js'
import orderModel from '../models/orderModel.js'

//get all vouchers
const getAllVoucher = (req, res) => {
    
    //3 xử lý dữ liệu
    voucherModel.find()
        .then((data) => {
            res.status(200).json({
                vouchers: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `internal sever error: ${err}`
            })
        })
}

//get voucher by code
const getVoucherByCode = (req, res) => {
    //get data
    let code = req.params.code

    voucherModel.findOne({ code: code })
        .then((data) => {
            res.status(200).json({
                voucher: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `internal sever error: ${err}`
            })
        })
}


//get voucher by id
const getVoucherById = (req, res) => {
    //1 lấy dữ liệu
    let idVoucher = req.params.id

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idVoucher)) {
        return res.status(400).json({
            message: 'id invalid'
        })
    }

    //3. xử lý dữ liệu
    voucherModel.findById(idVoucher)
        .then((data) => {
            res.status(200).json({
                message: 'successful to get voucher by Id',
                voucher: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `internal sever error: ${err}`
            })
        })
}




//get voucher of order
const getVoucherOfOrder = (req, res) => {
    //1. get data
    let idOrder = req.params.idOrder

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idOrder)) {
        return res.status(400).json({
            message: 'idOrder invalid'
        })
    }

    //3. process
    orderModel.findById(idOrder)
        .populate('voucher')
        .then((order) => {
            if (!order.voucher) {
                return res.status(404).json({
                    message: 'Voucher does not exist',
                })
            } else {
                res.status(200).json({
                    message: 'get voucher of order',
                    voucherInfo: order.voucher
                })
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `internal sever error: ${err}`
            })
        })
}

//creat voucher
const creatVoucher = (req, res) => {
    //1. lấy dữ liệu
    let body = req.body

    //.2 validate
    if (!Number.isInteger(body.discount)) {
        return res.status(400).json({
            message: 'discount invalid'
        })
    }

    //3. xử lý
    let newVoucher = new voucherModel({
        _id: mongoose.Types.ObjectId(),
        discount: body.discount
    })
    voucherModel.create(newVoucher)
        .then((data) => {
            res.status(201).json({
                message: 'Successful to creat new voucher!',
                voucher: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `internal server error: ${err}`
            })
        })
}

//update voucher
const updateVoucherById = (req, res) => {
    //1 lấy dữ liệu
    let idVoucher = req.params.id
    let body = req.body

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idVoucher)) {
        return res.status(400).json({
            message: 'id invalid'
        })
    }
    if (!body.discount) {
        return res.status(400).json({
            message: 'discount required'
        })
    }
    if (!Number.isInteger(body.discount)) {
        return res.status(400).json({
            message: 'discount invalid'
        })
    }


    //3. xử lý dữ liệu
    let newVoucher = new voucherModel({
        discount: body.discount
    })
    voucherModel.findByIdAndUpdate(idVoucher, newVoucher)
        .then((data) => {
            res.status(201).json({
                message: 'successful to update voucher by Id',
                prevVoucher: data,
                newVoucher: newVoucher
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `internal sever error: ${err}`
            })
        })


}

//deleteVoucherById 
const deleteVoucherById = (req, res) => {
    //1 lấy dữ liệu
    let { idVoucher } = req.params

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idVoucher)) {
        return res.status(400).json({
            message: 'idVoucher invalid'
        })
    }
    //3 xử lý dữ liệu

    voucherModel.findByIdAndDelete(idVoucher)
        .then((data) => {
            res.status(204).json({
                message: 'successful to delete voucher by Id',
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `internal sever error: ${err}`
            })
        })
}

export { creatVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById, getVoucherOfOrder, getVoucherByCode }