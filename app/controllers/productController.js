import mongoose from "mongoose";
import productModel from "../models/productModel.js";
import orderDetailModel from "../models/orderDetailModel.js";

//creat product 
const creatProduct = (req, res) => {
    //1. get data
    let body = req.body

    //2. validate
    if (!body.name) {
        return res.status(400).json({
            message: 'name is required'
        })
    }
    if (Number.isInteger(body.name)) {
        return res.status(400).json({
            message: 'name is invalid'
        })
    }
    if (body.description != undefined && Number.isInteger(body.description)) {
        return res.status(400).json({
            message: 'description is invalid'
        })
    }
    if (!body.type) {
        return res.status(400).json({
            message: 'type is required'
        })
    }
    if (!body.brand) {
        return res.status(400).json({
            message: 'brand is required'
        })
    }
    if (!body.color) {
        return res.status(400).json({
            message: 'color is required'
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            message: 'type is invalid'
        })
    }
    if (!body.imageUrl) {
        return res.status(400).json({
            message: 'imageUrl is required'
        })
    }
    if (Number.isInteger(body.imageUrl)) {
        return res.status(400).json({
            message: 'imageUrl is invalid'
        })
    }
    if (!body.buyPrice) {
        return res.status(400).json({
            message: 'buyPrice is required'
        })
    }
    if (!body.promotionPrice) {
        return res.status(400).json({
            message: 'promotionPrice is required'
        })
    }
    if (body.amount != undefined && !Number.isInteger(+body.amount)) {
        return res.status(400).json({
            message: 'amount is invalid'
        })
    }
    //3. process
    let newProduct = new productModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        brand: body.brand,
        color: body.color,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    })
    productModel.create(newProduct)
        .then((data) => {
            res.status(201).json({
                message: 'creat new product successfully!',
                product: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get all product 
const getAllProduct = (req, res) => {
    //1. get data
    console.log('line 102', req.query)
    let { limit, random, page, onlyActive } = req.query

    let filterCondition = {}
    if (onlyActive == 'true') {
        filterCondition.active = true
    } else if (onlyActive == 'false') {
        filterCondition.active = false
    }
    //3. process
    productModel.find({ filterCondition })
        .populate('type')
        .then((data) => {
            if (+limit > 0) {
                let newData
                if (random === 'true') {
                    let fromIndex = Math.floor(Math.random() * (data.length - +limit))
                    newData = data.slice(fromIndex, fromIndex + +limit)
                } else {
                    newData = data.slice(0, +limit)
                }
                return res.status(201).json({
                    data: newData
                })
            } else {
                res.status(201).json({
                    data
                })
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get all product pagination
const getAllProductPagination = (req, res) => {
    //1. get data
    let { limit, page } = req.query


    //3. process
    productModel.find()
        .populate('type')
        .limit(limit)
        .skip(limit * (page - 1))
        .then((productList) => {
            productModel.countDocuments()
                .then((count) => {
                    let numberPage = Math.ceil(+count / +limit)
                    console.log('numberPage get all product paionation', numberPage)
                    return res.status(201).json({
                        message: 'get product pagination successfully',
                        productList,
                        numberPage
                    })
                })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err}`
                    })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get product by filter
const getProductByFilter = (req, res) => {
    let filterConditionQuery = req.query
    let brand = !filterConditionQuery.brand ? [] : filterConditionQuery.brand.split(',')
    let color = !filterConditionQuery.color ? [] : filterConditionQuery.color.split(',')
    let priceMin = !filterConditionQuery.min ? 0 : +filterConditionQuery.min
    let priceMax = !filterConditionQuery.min ? 0 : +filterConditionQuery.max
    let page = !filterConditionQuery.page ? 1 : filterConditionQuery.page
    let limit = !filterConditionQuery.limit ? 10 : filterConditionQuery.limit
    let nameSort = filterConditionQuery.nameSort ? filterConditionQuery.nameSort : ''
    let priceSort = filterConditionQuery.priceSort ? filterConditionQuery.priceSort : ''
    let soldSort = filterConditionQuery.soldSort ? filterConditionQuery.soldSort : ''
    let name = filterConditionQuery.keyword
    let type = filterConditionQuery.type ? filterConditionQuery.type : ''
    let onlyActive = filterConditionQuery.onlyActive ? filterConditionQuery.onlyActive : ''

    let filterCondition = {
        promotionPrice: { $gte: priceMin }
    }
    if (onlyActive == 'true') {
        filterCondition.active = true
    } else if (onlyActive == 'false') {
        filterCondition.active = false
    }
    // console.log('onlyActive', onlyActive)
    // console.log('filterCondition.active', filterCondition.active)
    if (type !== '') {
        filterCondition.type = type
    }
    if (brand.length !== 0) {
        let newBrand = brand.map((brandItem) => {
            return brandItem = brandItem[0].toUpperCase() + brandItem.substring(1)
        })
        if (newBrand.length > 1) {
            filterCondition.brand = newBrand
        } else {
            filterCondition.brand = { "$regex": newBrand[0], "$options": "i" }
        }
    }
    if (color.length !== 0) {
        filterCondition.color = color
    }
    if (priceMax !== 0) {
        filterCondition.promotionPrice = { ...filterCondition.promotionPrice, $lte: priceMax }
    }
    if (name) {
        filterCondition.name = { "$regex": name, "$options": "i" }
    }

    let sortCondition = {}

    if (nameSort !== '') {
        sortCondition.name = +nameSort
    }
    if (priceSort !== '') {
        sortCondition.promotionPrice = +priceSort
    }
    if (soldSort !== '') {
        sortCondition.quantitySold = +soldSort
    }
    productModel.find()
        .then((dataOrigin) => {
            //start: filter all brand and color
            let productListOrigin = dataOrigin.map((product, index) => {
                return {
                    brand: product.brand,
                    color: product.color
                }
            })

            productListOrigin.forEach((product, index) => {
                for (let i = index + 1; i < productListOrigin.length; i++) {
                    if (productListOrigin[i].brand === product.brand) {
                        productListOrigin[i].brand = 'out'
                    }
                    if (productListOrigin[i].color === product.color) {
                        productListOrigin[i].color = 'out'
                    }
                }
            })
            const brandsListOrigin = productListOrigin
                .map((product) => {
                    return product.brand
                })
                .filter((brand) => brand !== 'out')

            const colorListOrigin = productListOrigin
                .map((product) => {
                    return product.color
                })
                .filter((color) => color !== 'out')
            //end: filter all brand and color


            productModel.find(filterCondition)
                .then((dataFilter) => {
                    let totalProducts = dataFilter.length
                    let totalQuantitySold = dataFilter.reduce((acc, product) => {
                        return acc += +product.quantitySold
                    }, 0)
                    productModel.find(filterCondition)
                        .populate('type')
                        .sort(sortCondition)
                        .limit(limit)
                        .skip(limit * (page - 1))
                        .then((dataPage) => {
                            console.log('getProductByFilter')
                            let productListFilter = dataFilter.map((product, index) => {
                                return {
                                    brand: product.brand,
                                    color: product.color
                                }
                            })

                            productListFilter.forEach((product, index) => {
                                for (let i = index + 1; i < productListFilter.length; i++) {
                                    if (productListFilter[i].brand === product.brand) {
                                        productListFilter[i].brand = 'out'
                                    }
                                    if (productListFilter[i].color === product.color) {
                                        productListFilter[i].color = 'out'
                                    }
                                }
                            })
                            const brandsListFilter = productListFilter
                                .map((product) => {
                                    return product.brand
                                })
                                .filter((brand) => brand !== 'out')

                            const colorListFilter = productListFilter
                                .map((product) => {
                                    return product.color
                                })
                                .filter((color) => color !== 'out')
                            let numberPage = Math.ceil(+dataFilter.length / +limit)
                            let brandsList
                            let colorList
                            if (filterCondition.name) {
                                brandsList = brandsListFilter
                                colorList = colorListFilter
                            } else {
                                brandsList = brandsListOrigin
                                colorList = colorListOrigin
                            }
                            return res.status(200).json({
                                productList: dataPage,
                                brandsList,
                                colorList,
                                overal: {
                                    totalProducts,
                                    totalQuantitySold
                                },
                                numberPage
                            })
                        })
                        .catch((err) => {
                            res.status(500).json({
                                message: `Internal sever error: ${err}`
                            })
                        })
                })
                .catch((err) => {
                    res.status(500).json({
                        message: `Internal sever error: ${err}`
                    })
                })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })

}

//get product by id
const getProductById = (req, res) => {
    //1. get data
    let idProduct = req.params.id

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idProduct)) {
        return res.status(400).json({
            message: 'id is invalid'
        })
    }

    //3. process
    productModel.findById(idProduct)
        .populate('type')
        .then((data) => {
            res.status(201).json({
                message: 'get product by id successfully!',
                product: data
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get type of product
const getTypeOfProduct = (req, res) => {
    //1. get data
    let idProduct = req.params.id

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idProduct)) {
        return res.status(400).json({
            message: 'id is invalid'
        })
    }

    //3. process
    productModel.findById(idProduct)
        .populate('type')
        .then((product) => {
            res.status(200).json({
                message: 'get type of product successfully',
                typeProduct: product.type
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get product of order detail
const getProductOfOrderDetail = (req, res) => {
    //1. get data
    let idOrderDetail = req.params.idOrderDetail

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idOrderDetail)) {
        return res.status(400).json({
            message: 'idOrderDetail is invalid'
        })
    }

    //3. process
    orderDetailModel.findById(idOrderDetail)
        .populate('product')
        .then((orderDetail) => {
            res.status(200).json({
                message: 'get product of order detail successfully',
                product: orderDetail.product
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//get number of total product
const getNumberOfTotalProduct = (req, res) => {
    productModel.find()
        .then((products) => {
            res.status(200).json({
                message: 'get number of total product successfully',
                numberOfTotalProduct: products.length
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//update product 
const updateProduct = (req, res) => {
    //1. get data
    let idProduct = req.params.id
    let body = req.body

    //.2 validate
    if (!mongoose.Types.ObjectId.isValid(idProduct)) {
        return res.status(400).json({
            message: 'id is invalid'
        })
    }
    if (body.name != undefined && Number.isInteger(body.name)) {
        return res.status(400).json({
            message: 'name is invalid'
        })
    }
    if (body.description != undefined && Number.isInteger(body.description)) {
        return res.status(400).json({
            message: 'description is invalid'
        })
    }
    if (body.type != undefined && !mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            message: 'type is invalid'
        })
    }
    if (body.imageUrl != undefined && Number.isInteger(body.imageUrl)) {
        return res.status(400).json({
            message: 'imageUrl is invalid'
        })
    }

    //3. process
    let newProduct = {
        name: body.name,
        description: body.description,
        brand: body.brand,
        color: body.color,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
    }

    if (body.active !== '') {
        newProduct.active = body.active
    }

    console.log('newProduct', newProduct)
    console.log('body.active', typeof body.active)
    productModel.findByIdAndUpdate(idProduct, newProduct)
        .then((prevProduct) => {
            res.status(201).json({
                message: 'update product by id successfully!',
                prevProduct: prevProduct,
                newProduct: newProduct
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//check buy quantity of orders vs stock quantity of product
const getResultValidateBuyQuantity = (req, res) => {
    //1. get data
    let { productId, buyQuantity } = req.params

    productModel.findById(productId)
        .then(productById => {
            console.log('+productById.amount < +buyQuantity', +productById.amount < +buyQuantity)
            if (+productById.amount < +buyQuantity) {
                return res.status(200).json({
                    message: `getResultValidateBuyQuantity successfully`,
                    result: false,
                    description: 'the buy quantity larger than the quantity of stock',
                    product: productById,
                    buyQuantity
                })
            } else {
                return res.status(200).json({
                    message: `getResultValidateBuyQuantity successfully`,
                    result: true,
                    description: 'the buy quantity less than the quantity of stock'
                })
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

//delete product 
const deleteProduct = (req, res) => {
    //1. get data
    let idProduct = req.params.id

    //2. validate
    if (!mongoose.Types.ObjectId.isValid(idProduct)) {
        return res.status(400).json({
            message: 'id is invalid'
        })
    }

    //3. process
    productModel.findByIdAndDelete(idProduct)
        .then((data) => {
            res.status(204).json({
                message: 'delete product by id successfully!',
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}

export {
    creatProduct,
    getAllProduct,
    getProductById,
    getTypeOfProduct,
    getProductOfOrderDetail,
    updateProduct,
    deleteProduct,
    getProductByFilter,
    getNumberOfTotalProduct,
    getAllProductPagination,
    getResultValidateBuyQuantity
}