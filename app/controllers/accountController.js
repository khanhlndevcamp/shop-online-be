import mongoose from "mongoose";
import accountModel from "../models/accountModel.js";
import customerModel from "../models/customerModel.js";
import bcrypt from 'bcryptjs'

//create account
export const createAccount = (req, res) => {
    //get data
    let body = req.body

    //2. validate
    if (!body.fullName) {
        return res.status(400).json({
            message: 'fullName is required'
        })
    }
    if (Number.isInteger(body.fullName)) {
        return res.status(400).json({
            message: 'fullName is invalid'
        })
    }
    if (!body.phone) {
        return res.status(400).json({
            message: 'phone is required'
        })
    }
    if (Number.isInteger(body.phone)) {
        return res.status(400).json({
            message: 'phone is invalid'
        })
    }
    if (!body.email) {
        return res.status(400).json({
            message: 'email is required'
        })
    }

    //process
    let newCustomer = new customerModel({
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address
    })

    customerModel.create(newCustomer)
        .then(dataCustomer => {

            const newAccount = new accountModel({
                _id: mongoose.Types.ObjectId(),
                username: body.username,
                password: bcrypt.hashSync(body.password, 8),
                customer: dataCustomer._id
            })

            if(body.role !== '') {
                newAccount.role = body.role
            }

            if (body.imageURL !== '') {
                newAccount.imageURL = body.imageURL
            }

            accountModel.create(newAccount)
                .then(dataAccount => {
                    res.status(201).json({
                        message: 'create new account successfully',
                        account: dataAccount,
                        customer: dataCustomer
                    })
                })
                .catch(err => {
                    res.status(500).json({
                        message: `Internal server error: ${err}`
                    })
                })
        })
        .catch(err => {
            res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}

export const getResultCheckDupUsername = (req, res) => {
    //get data
    const username = req.params.username
    const currentUsername = req.query.currentUsername ? req.query.currentUsername : '' //use to edit customer

    accountModel.findOne({ username })
        .then((user) => {
            if (username === currentUsername) {
                return res.status(200).json({
                    message: 'username valid',
                    result: true
                })
            } else {
                if (user) {
                    return res.status(200).json({
                        message: 'username already exist',
                        result: false
                    })
                } else {
                    return res.status(200).json({
                        message: 'username valid',
                        result: true
                    })
                }
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}

//get all account
export const getAllAccount = (req, res) => {
    accountModel.find()
        .populate('customer')
        .then(data => {
            res.status(200).json({
                message: `Get all account successfully`,
                account: data
            })
        })
        .catch(err => {
            res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}

//get account by customer Id
export const getAccountByCustomerId = (req, res) => {
    //get data
    let customerId = req.params.customerId

    accountModel.findOne({ customer: customerId })
        .populate('customer')
        .then(data => {
            res.status(200).json({
                message: `Get account by customer id successfully`,
                // account: {
                //     // imageURL: data.imageURL,
                //     // role: data.role,
                //     // customer: data.customer,
                //     data
                // },
                account: data
            })
        })
        .catch(err => {
            res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}

//get account by id
export const getAccountById = (req, res) => {
    //get data
    let accountId = req.accountId
    accountModel.findById(accountId)
        .populate('customer')
        .then(data => {
            res.status(200).json({
                message: `Get account by id successfully`,
                account: {
                    imageURL: data.imageURL,
                    role: data.role,
                    customer: data.customer
                }
            })
        })
        .catch(err => {
            res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}

export const updateAccountById = (req, res) => {
    let body = req.body
    let accountId = req.params.accountId

    console.log('187, body req', body)
    //2. validate
    if (body.fullName && Number.isInteger(body.fullName)) {
        return res.status(400).json({
            message: 'fullName is invalid'
        })
    }

    if (body.phone && Number.isInteger(body.phone)) {
        return res.status(400).json({
            message: 'phone is invalid'
        })
    }

    //process
    const newAccount = {
        username: body.username,
        // password: bcrypt.hashSync(body.password, 8),
        // imageURL: body.imageURL,
        role: body.role,
    }
    if (body.imageURL !== '') {
        newAccount.imageURL = body.imageURL
    }
    if (body.password !== '' && body.password) {
        console.log('line 211: account controller: update password')
        newAccount.password = bcrypt.hashSync(body.password, 8)
    }
    console.log('211 new account', newAccount)
    accountModel.findByIdAndUpdate(accountId, newAccount)
        .then(prevAccount => {
            let customerId = prevAccount.customer
            console.log('215 customer id', customerId)
            let newCustomer = {
                fullName: body.fullName,
                phone: body.phone,
                email: body.email,
                address: body.address
            }
            console.log('222 new customer', newCustomer)
            customerModel.findByIdAndUpdate(customerId, newCustomer)
                .then(prevCustomer => {
                    res.status(201).json({
                        message: 'update account & customer successfullt',
                        prevAccount,
                        newAccount,
                        prevCustomer,
                        newCustomer
                    })
                })
                .catch(err => {
                    res.status(500).json({
                        message: `Internal server error: ${err}`
                    })
                })
        })
        .catch(err => {
            res.status(500).json({
                message: `Internal server error: ${err}`
            })
        })
}

export const deleteAccountById = (req, res) => {
    //get data
    let accountId = req.params.accountId

    accountModel.findByIdAndDelete(accountId)
        .then((updatedOrder) => {
            res.status(204).json({
                message: 'delete account by id successfully!',
            })
        })
        .catch((err) => {
            res.status(500).json({
                message: `Internal sever error: ${err}`
            })
        })
}