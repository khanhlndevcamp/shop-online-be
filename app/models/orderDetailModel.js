import mongoose from "mongoose";

//get schema
const Schema = mongoose.Schema

//creat model
const orderDetailModel = new Schema({
    _id: mongoose.Types.ObjectId,
    product: {
        type: mongoose.Types.ObjectId,
        ref: 'product'
    },
    quantity: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
})

export default mongoose.model('orderDetail', orderDetailModel)