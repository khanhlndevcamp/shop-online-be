import mongoose from "mongoose";

//get schema
const Schema = mongoose.Schema

//creat model
const accountModel = new Schema({
    _id: mongoose.Types.ObjectId,
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    imageURL: {
        type: String,
        default: 'https://img.freepik.com/free-icon/user_318-159711.jpg?w=2000'
    },  
    role: {
        type: String,
        require: true,
        default: 'user'
    },
    customer: {
        type: mongoose.Types.ObjectId,
        ref: 'customer'
    }
}, {
    timestamps: true
})

export default mongoose.model('account', accountModel)
