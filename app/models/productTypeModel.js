import mongoose from "mongoose";

//get schema
const Schema = mongoose.Schema

//creat model
const productTypeModel = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: String
}, {
    timestamps: true
})

export default mongoose.model('productType', productTypeModel)