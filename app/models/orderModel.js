import mongoose from "mongoose";
import randomToken from "random-token";

//get schema
const Schema = mongoose.Schema

//creat model
const orderModel = new Schema({
    _id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: Date.now()
    },
    orderCode: {
        type: String,
        required: true,
        default: () => {
            return randomToken(8)
        }
    },
    shippedDate: Date,
    note: String,
    orderDetails: [{
        type: mongoose.Types.ObjectId,
        ref: 'orderDetail'
    }],
    cost: {
        type: Number,
        default: 0
    },
    totalQuantity: {
        type: Number,
        default: 0
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: 'voucher'
    },
    status: {
        type: String,
        required: true,
        default: 'waiting for confirmation'
    },
    customerName: {
        type: String,
        required: true
    },
    phoneNumber: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    progressPercent: {
        type: Number,
        required: true,
        default: 23
    },
    step: {
        type: Number,
        required: true,
        default: 2
    }
}, {
    timestamps: true
})

export default mongoose.model('order', orderModel)