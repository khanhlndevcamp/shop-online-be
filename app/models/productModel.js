import mongoose from "mongoose";

//get schema
const Schema = mongoose.Schema

//creat model
const productModel = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: String,
    brand: {
        type: String,
        required: true
    },
    color:  {
        type: String,
        required: true
    },
    type: {
        type: mongoose.Types.ObjectId,
        ref: 'productType',
        required: true
    },
    imageUrl: {
        type: String,
        requried: true
    },
    buyPrice: {
        type: Number,
        required: true
    },
    promotionPrice: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        default: 0
    },
    quantitySold: {
        type: Number,
        default: 0
    },
    orderDetails: [{
        type: mongoose.Types.ObjectId,
        ref: 'orderDetail'
    }],
    active: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
})

export default mongoose.model('product', productModel)