import mongoose from "mongoose";
import randomToken from "random-token";

const Schema = mongoose.Schema

//create model

const voucherModel = new Schema({
    _id: mongoose.Types.ObjectId,
    code: {
        type: String,
        uniqued: true,
        default: () => {
           return randomToken(8)
        }
    },
    discount: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
})

export default mongoose.model('voucher', voucherModel)