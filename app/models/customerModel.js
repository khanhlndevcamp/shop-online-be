import mongoose from "mongoose";

//get schema
const Schema = mongoose.Schema

//creat model
const customerModel = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        default: ""
    },
    city: {
        type: String,
        default: ""
    },
    country: {
        type: String,
        default: ""
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: 'order'
    }],
    totalOrder: {
        type: Number,
        default: 0
    },
    totalReveneu: {
        type: Number,
        default: 0
    },
    active: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true
})

export default mongoose.model('customer', customerModel)
