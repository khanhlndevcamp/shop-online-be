import express from "express"
import { login, checkExpiredDateToken } from "../controllers/loginController.js"

//initialize router
const loginRouter = express.Router()

//post login
loginRouter.post('/login', login)

//check expired date access token
loginRouter.get('/check-expired', checkExpiredDateToken)

export default loginRouter
 