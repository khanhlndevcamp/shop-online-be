import express from "express"

import {creatOrderDetail, getAllOrderDetail, getOrderDetailById, getOrderDetailOfOrder, updateOrderDetail, deleteOrderDetail} from "../controllers/orderDetailController.js"
import { authentication } from "../middlewares/authentication.js"
import { authorization } from "../middlewares/authorization.js"
//initialize router
const orderDetailRouter = express.Router()

//creat order detail
orderDetailRouter.post('/orders/:idOrder/order-details', authentication, authorization(['user','admin']), creatOrderDetail)

//get all order detail : not use in project
orderDetailRouter.get('/order-details',authentication, getAllOrderDetail)

//get order detail by id : not use in project
orderDetailRouter.get('/order-details/:idOrderDetail',authentication, getOrderDetailById)

//get order detail of order : not use in project
orderDetailRouter.get('/orders/:idOrder/order-details',authentication, getOrderDetailOfOrder)

//update order detail
orderDetailRouter.put('/order-details/:idOrderDetail',authentication, authorization(['admin']), updateOrderDetail)

//delete order detail
orderDetailRouter.delete('/orders/:idOrder/order-details/:idOrderDetail', authentication, deleteOrderDetail)

export default orderDetailRouter
