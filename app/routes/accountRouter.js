import express from "express"
import { 
    createAccount, 
    getAllAccount, 
    getAccountById, 
    getAccountByCustomerId,
    getResultCheckDupUsername,
    updateAccountById,
    deleteAccountById
} from "../controllers/accountController.js"
import { authentication } from "../middlewares/authentication.js"
import { authorization } from "../middlewares/authorization.js"
import { validateDupUsername } from "../middlewares/Account/validateDupUsername.js"
import { validateDupPhoneNumber } from "../middlewares/Account/validateDupPhoneNumber.js"
import { validateDupEmail } from "../middlewares/Account/validateDupEmail.js"

//initialize router
const accountRouter = express.Router()

//creat account
accountRouter.post('/account', validateDupUsername, validateDupPhoneNumber, validateDupEmail, createAccount)

//create account choose role
accountRouter.post('/account/choose-role', authentication, authorization(['admin']), validateDupUsername, validateDupPhoneNumber, validateDupEmail, createAccount)

//get all account
accountRouter.get('/accounts', authentication, authorization(['admin']), getAllAccount)

//get account by customer Id
accountRouter.get('/account/customer/:customerId', authentication, authorization(['admin']), getAccountByCustomerId )

//get account by Id
accountRouter.get('/account', authentication,  getAccountById)

//get result check dup username
accountRouter.get('/account/check/username/:username', getResultCheckDupUsername)

//update account
accountRouter.put('/account/choose-role/:accountId', authentication, authorization(['admin']), updateAccountById)

//delete account
accountRouter.delete('/account/:accountId', authentication, authorization(['admin']), deleteAccountById )

export default accountRouter
