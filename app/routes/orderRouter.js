import express from "express"

//import controller
import {creatOrder, 
    getAllOrder, 
    getOrderById, 
    getOrderByOrderCode, 
    getOrdersOfCustomer, 
    getOrdersOfCustomerByCustomerEmail, 
    updateOrder, 
    deleteOrder,
    getOrderOverview,
    getOrderByFitler,
    getTotalReveneuOfOrderOfEachMonth,
    getTotalReveneuOfOrderOfEachDay
} from "../controllers/orderController.js"

//import middleware
import {validateOrderDate, validateShippedDate} from '../middlewares/orderMiddleware.js'
import { authentication } from "../middlewares/authentication.js"
import { authorization } from "../middlewares/authorization.js"
//initialize router
const orderRouter = express.Router()

//creat order
orderRouter.post('/customers/:idCustomer/orders', authentication, authorization(['user', 'admin']), validateOrderDate, validateShippedDate, creatOrder)

//get all order
orderRouter.get('/orders', authentication, authorization(['admin']), getAllOrder)

//get all order by filter
orderRouter.get('/orders/filter', authentication, authorization(['admin']), getOrderByFitler)

//get order overview
orderRouter.get('/orders/overview', authentication, authorization(['admin']), getOrderOverview) 

//get order by order code
orderRouter.get('/orders/code/:orderCode', authentication, getOrderByOrderCode)

//get order by id
orderRouter.get('/orders/:idOrder', authentication, getOrderById)

//get order of customer
orderRouter.get('/customers/:idCustomer/orders', authentication, getOrdersOfCustomer)

//get total reveneu of order of each month
orderRouter.get('/orders/month/:year',authentication, authorization(['admin']), getTotalReveneuOfOrderOfEachMonth)

//get total reveneu of order of each day
orderRouter.get('/orders/day/:year/:month',authentication, authorization(['admin']), getTotalReveneuOfOrderOfEachDay)

//get order of customer by customer email
orderRouter.get('/customers/email/:emailCustomer/orders', authentication, getOrdersOfCustomerByCustomerEmail)

//update order
orderRouter.put('/orders/:idOrder', validateOrderDate, validateShippedDate, authentication, authorization(['admin']), updateOrder)

//delete order
orderRouter.delete('/customers/:idCustomer/orders/:idOrder',authentication, deleteOrder)


export default orderRouter
