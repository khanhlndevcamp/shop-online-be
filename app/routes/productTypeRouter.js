import express from "express"

import {creatProductType, getAllProductType, getProductTypeById, updateProductType, deleteProductType} from "../controllers/productTypeController.js"
import { authentication } from "../middlewares/authentication.js"
import { authorization } from "../middlewares/authorization.js"
//initialize router
const productTypeRouter = express.Router()

//creat product type
productTypeRouter.post('/product-type', authentication, authorization(['admin']), creatProductType)

//get all product type
productTypeRouter.get('/product-type', authentication, authorization(['admin']), getAllProductType)

//get product type by id : not use in project
productTypeRouter.get('/product-type/:id',authentication, authorization(['admin']), getProductTypeById)

//update product type : not use in project
productTypeRouter.put('/product-type/:id', authentication, authorization(['admin']), updateProductType)

//delete product type : not use in project
productTypeRouter.delete('/product-type/:id', authentication, authorization(['admin']),deleteProductType)

export default productTypeRouter
