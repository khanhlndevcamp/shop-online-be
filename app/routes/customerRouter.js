import express from "express"

import {
    creatCustomer, 
    getAllCustomer, 
    getCustomerById, 
    updateCustomer, 
    deleteCustomer, 
    getCustomerByPhoneNumber,
    getNumberOfTotalCustomer,
    getCustomerByOrderId,
    getCustomerByFitler,
    getResultCheckDupEmail,
    getResultCheckDupPhone
} from "../controllers/customerController.js"
import { authentication } from "../middlewares/authentication.js"
import { authorization } from "../middlewares/authorization.js"
//initialize router
const customerRouter = express.Router()

//creat customer
customerRouter.post('/customers', creatCustomer)

//get all customer
customerRouter.get('/customers',authentication, authorization(['admin']),  getAllCustomer)

//get number of total customer
customerRouter.get('/customers/total', authentication, authorization(['admin']), getNumberOfTotalCustomer)

//get customer by order id
customerRouter.get('/customers/order/:orderId', authentication, authorization(['admin']), getCustomerByOrderId)

//get customer by phone number : not use in project
customerRouter.get('/customers/phone/:phoneNumber',authentication, getCustomerByPhoneNumber)

//get all order by filter
customerRouter.get('/customers/filter', authentication, authorization(['admin']), getCustomerByFitler)

//get customer by id
customerRouter.get('/customers/:id',authentication, getCustomerById)

//get result check dup email
customerRouter.get('/customers/check/email/:email', getResultCheckDupEmail)

//get result check dup phone
customerRouter.get('/customers/check/phone/:phone', getResultCheckDupPhone)

//update customer : not use in project
customerRouter.put('/customers/:id',authentication, authorization(['admin']), updateCustomer)

//delete customer
customerRouter.delete('/account/:accountId/customers/:customerId', authentication, authorization(['admin']), deleteCustomer)

export default customerRouter
