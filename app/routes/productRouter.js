import express from "express"

import {
    creatProduct, 
    getAllProduct, 
    getProductById, 
    getTypeOfProduct, 
    getProductOfOrderDetail, 
    updateProduct, 
    deleteProduct, 
    getProductByFilter,
    getNumberOfTotalProduct,
    getAllProductPagination,
    getResultValidateBuyQuantity
} from "../controllers/productController.js"
import { authentication } from "../middlewares/authentication.js"
import { authorization } from "../middlewares/authorization.js"
//initialize router
const productRouter = express.Router()

//creat product
productRouter.post('/products',authentication, authorization(['admin']), creatProduct)

//check buy quantity of orders vs stock quantity of product
productRouter.get('/products/validate-buy-quantity/:productId/:buyQuantity', getResultValidateBuyQuantity)

//get all product
productRouter.get('/products', getAllProduct)

//get all product pagination
productRouter.get('/products/pagination', getAllProductPagination)

//get number of total product
productRouter.get('/products/total', authentication, authorization(['admin']), getNumberOfTotalProduct)

//get product by filter
productRouter.get('/products/filter', getProductByFilter)

//get product by id
productRouter.get('/products/:id', getProductById)

//get type of product
productRouter.get('/products/:id/product-type', getTypeOfProduct)

//get product of order detail
productRouter.get('/order-details/:idOrderDetail/products', getProductOfOrderDetail)

//update product
productRouter.put('/products/:id',authentication, authorization(['admin']), updateProduct)

//delete product
productRouter.delete('/products/:id',authentication, authorization(['admin']), deleteProduct)

export default productRouter
