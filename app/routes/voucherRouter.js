import express from 'express'
import {creatVoucher, getAllVoucher, getVoucherByCode, getVoucherById, updateVoucherById, deleteVoucherById, getVoucherOfOrder} from '../controllers/voucherController.js'
import { authentication } from '../middlewares/authentication.js'
import { authorization } from '../middlewares/authorization.js'

const voucherRouter = express.Router()

//get all voucher : not use in project
voucherRouter.get('/vouchers', authentication, authorization(['admin']), getAllVoucher)

//get voucher by id : not use in project
voucherRouter.get('/vouchers/:id', getVoucherById)

//get voucher by voucher code
voucherRouter.get('/vouchers/code/:code', getVoucherByCode)

//get voucher of order : not use in project
voucherRouter.get('/orders/:idOrder/vouchers', authentication, getVoucherOfOrder)

//update voucher by id : not use in project
voucherRouter.put('/vouchers/:id', authentication, authorization(['admin']), updateVoucherById)

//create voucher : not use in project
voucherRouter.post('/vouchers', authentication, authorization(['admin']), creatVoucher)

//delete voucher : not use in project
voucherRouter.delete('vouchers/:idVoucher', authentication, authorization(['admin']), deleteVoucherById)


export default voucherRouter
